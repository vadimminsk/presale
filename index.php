<?php
require_once 'vendor/autoload.php';
use GeoIp2\Database\Reader;

// This creates the Reader object, which should be reused across
// lookups.
$reader = new Reader('GeoLite2-Country.mmdb');

// Replace "city" with the appropriate method for your database, e.g.,
// "country".
$record = $reader->country('128.101.101.101');

// print($record->country->isoCode . "\n"); // 'US'
// print($record->country->name . "\n"); // 'United States'

if ($record->country->isoCode == 'ru') {
	include 'index-ru.html';
} else {
	include 'index-en.html';
}