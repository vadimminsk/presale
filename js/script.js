
//Смена изображения на гифку при наведении
// $(".content_gallery__images--item").hover(function(e) {
//     $(this).find('.content_gallery__images--png').toggle();
//     $(this).find('.content_gallery__images--gif').toggle();
// });

//Slider
$(document).ready(function(){
    $('.content_slider-block__phone--images').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 1,
        slidesToScroll: 1
    });
});
$(document).ready(function(){
    //Description Slider
    $('.content_description__phone--images').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 1,
        slidesToScroll: 1
    });
});


//Parallax

$(window).stellar({
    verticalOffset: 10,
    horizontalScrolling: false
});

// Video
$(document).ready(function(){
    $('.content_description__video').click(function(){
        $('.video').trigger( $('.video').prop('paused') ? 'play' : 'pause');
        $('.content_description__phone--button').toggleClass('video_button');
    });
});

// Video button

$(document).ready(function () {
   $('.content_description__phone--button').click(function () {
       $('.video').trigger( $('.video').prop('paused') ? 'play' : 'pause');
       $(this).toggleClass('video_button');
   });
   $('.content_description__phone--button').click(function(){
        $('.video').trigger( $('.video').prop('paused') ? 'play' : 'pause');
    });
});
    
